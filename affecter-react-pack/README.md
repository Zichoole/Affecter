
[本项目的Git仓库]: https://gitee.com/guobinyong/Affecter.git
[Affecter的教程]: https://gitee.com/guobinyong/Affecter
[Affecter的接口文档]: https://gitee.com/guobinyong/Affecter/blob/master/doc/Affecter的接口文档.md
[内置Transforms的接口文档]: https://gitee.com/guobinyong/Affecter/blob/master/doc/内置Transforms的接口文档.md



# 一、简介
`Affecter` 中文名字是 `影响者`，它用于根据子元素与其与特定1个或者多位置的距离对子元素设置样式;

affecter-react 是 React版本的 Affecter ，将来还会有 Vue、Angular 等各大框架的版本；

在 [本项目的Git仓库][] 中包含了 affecter-react 和 下文的示例代码；点击[本项目的Git仓库][]，即可下载；


关于 Affecter 的详细信息，请参考以下文档：
- [Affecter的教程][]
- [Affecter的接口文档][]
- [内置Transforms的接口文档][]




**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe





# 二、安装方式
目前，安装方式有以下作几种：


## 方式1：通过 npm 安装
```
npm install --save affecter-react
```


## 方式2：直接下载原代码
您可直接从 [本项目的Git仓库][] 下载，然后直接把 [本项目的Git仓库][] 下的 affecter 目录拷贝到您的项目中去，并通过如下代码在您的项目中引入 Affecter、AffectedItem ：
```
import { Affecter, AffectedItem, computeLoopItemTypeCreater } from "path/to/affecter/Affecter.js";
```