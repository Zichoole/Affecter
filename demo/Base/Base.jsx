//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Base.css';

import img2 from '../assets/img2.png';




// 组件
class Base extends Component {

    constructor(props){
        super(props);

        this.transforms = [Transforms.abs,Transforms.magnifierCreater({x:150,y:150})];
    }


    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let dis = distanceArr[0];
        let magn = 150 * dis.y;
        return {transform:`perspective(500px) translateZ(${magn}px)`};
    }

    render() {

        let itemArr = [];
        for (let i = 1; i <= 300; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }


        // css布局
        return (
            <Affecter  className="affecter" wrapChildren={true}   affectAnchors={[{x:0.5,y:0.5}]} transforms={this.transforms} getItemAffectStyle={this.getItemAffectStyle}  >{itemArr}</Affecter>
        );

    }
}





//导出
export {Base};
