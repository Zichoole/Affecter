//导入
import React, { Component } from 'react';



import { Affecter, AffectedItem, computeLoopItemTypeCreater } from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './ComputeLoop.css';

import img2 from '../assets/img2.png';








class ItemContent extends Component {
    constructor(props){
        super(props);
        this.itemIndex = props.itemIndex;
    }

    shouldComponentUpdate(nextProps, nextState){
       return this.itemIndex != nextProps.itemIndex ;
    }

    render() {
        let {itemData} = this.props;
        return (
            <div >
                <img className="img" src={img2}/>
                <p>{itemData}</p>
            </div>
        );
    }

}



/* class ItemContent extends Component {
    render() {
        let { itemData } = this.props;
        return (
            <div >
                <img className="img" src={img2} />
                <p>{itemData}</p>
            </div>
        );
    }

} */

let ComputeLoopItem = computeLoopItemTypeCreater(ItemContent);


// 组件
class ComputeLoop extends Component {
    constructor(props) {
        super(props);

        let itemDataArr = [];

        for (let index = 0; index < 50; index++) {
            itemDataArr.push(index);
        }

        this.itemDataArr = itemDataArr;

        this.transforms = [Transforms.hypotenuse, Transforms.magnifierCreater({ x: 150, y: 150 })];
    }





    //影响样式
    getItemAffectStyle(distanceArr, itemElement, containerElement, itemRowCol, itemRect) {
        let scale = distanceArr[0].x;
        let translateZ = scale * 140;
        return { transform: `perspective(300px) translateZ(${translateZ}px)` };
    }




    render() {

        return (
            <div>
                <Affecter ItemType={ComputeLoopItem} itemDataArr={this.itemDataArr}  itemSize={{ width: 50, height: 50 }} horSpace={20} verSpace={20} roundRowCount={5} renderExtendRadius={100} loopType="All" className="affecter" affectAnchors={[{ x: 0.5, y: 0.5 }]} getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}></Affecter>
            </div>
        );

    }
}











//导出
export { ComputeLoop };

