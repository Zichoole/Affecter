//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Drill.css';

import img2 from '../assets/img2.png';






// 组件
class Drill extends Component {
    constructor(props) {
        super(props);

        this.clickHandle = this.clickHandle.bind(this);

        let newAnchor = this.creatAffectAnchor();
        let affectAnchors = [newAnchor];
        this.state = {
            affectAnchors:affectAnchors
        };


       
        this.transforms = [Transforms.drillHoleCreater({x:50,y:50},{x:200,y:200},{x:2000,y:200})];
    }





    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let distance = distanceArr[0];
        return {transform:`translate(${distance.offset.x}px, 0px) scale(${distance.scale.y},1)`};
    }


    clickHandle(event){
        let newAnchor = this.creatAffectAnchor();
        let affectAnchors = [newAnchor];
        this.setState({
            affectAnchors:affectAnchors
        });

    }

    creatAffectAnchor(){
        let random = Math.random;
        return {x:random(),y:random()};
    }

    render() {

        // css布局
        return (
            <div>
                <Affecter  className="affecter" wrapChildren={true}   affectAnchors={[{x:0.5,y:0.5}]}   getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}>
                    {
                        function(total){
                            let arr = [];
                            for (let i = 1;i <= total;i++) {
                                let elem = (
                                <AffectedItem key={i} className="affected-item">
                                </AffectedItem> 
                                );
                                arr.push(elem);
                            }
                            return arr;
                        }(300)
                    }
                </Affecter>
            </div>
        );

    }
}











//导出
export {Drill};

