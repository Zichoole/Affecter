内置Transforms的接口文档
==================




# 转换器的类型
`(distance:Coord,index:number,distanceArr:[Coord],itemRect :Rect,itemElement?:Element,containerElement?:Element)=>Coord`

- @this   :Object     用于保存各个距离坐标对象所共享的数据； this 的值会被作为 distanceArr.publicData 的值传给下一个转换器；
- @param distance : {x:number,y:number}    项目锚点与第index个影响锚点之间的距离坐标
- @param index : number       当前影响锚点的序号
- @param distanceArr : [{x:number,y:number}]      项目锚点与所有影响锚点的距离坐标数组，该数组有个 publicData 属性，publicData 属性中存储的是各个距离坐标对象共享的数据；
- @param itemRect :{x:number,y:number,width:number,height:number}    项目元素的位置和长宽信息
- @param itemElement ?:Element    项目元素的Dom节点
- @param containerElement ?:Element   项目元素的容器的Dom节点；
- @returns {x:number,y:number,...other}   转换后的距离坐标对象；


## 注意
- 转换器会按转换器数组中的顺序调用，上个转换器的返回值，会作为下个转换器的distance；
- 转换器中返回的对象必须包含 `x` 和 `y` 属性；
- 对于距离坐标对象的私有数据应该保存在距离坐标对象中；
- 对于不属于任何距离坐标的数据应该保存在 转换器中的 this 里；this 里的数据会被作为共享数据储存在 distanceArr.publicData 中；
- 通用的转换器应该把转换后的值与传入的 distance 合并，以达到覆盖distance的 `x` 和 `y` 属性，同时又能保留distance中的其它属性值；
   ```
   function transformeFun(distance) {
       //转换过程省略...

       let transformedResult = ...;     //转换结果

       let newDistance = { ...distance, ...transformedResult };     //合并 原距离坐标对象 和 转换结果
       return newDistance; 返回合并后的对象
   }
   ```
- 如果需要实现转换器的分支，则需要把转换的结果保存在 this中 或者 距离坐标对象中除 `x` 和 `y` 属性之外的其它属性中；





# 内置的转换器

## abs
- 说明：绝对值转换器


## magnifierCreater
`magnifierCreater(referencer, effectRange)`
- @param referencer ?: {x:number = 1,y:number = 1}  参考数据，相对数据
- @param effectRange ?: {x:number = Number.POSITIVE_INFINITY,y:number = Number.POSITIVE_INFINITY}  有效范围
- 说明：放大转换器生成器


## drillHoleCreater
`drillHoleCreater(radius, drillDistance, effectRange)`
- @param radius ?: {x:number = 0,y:number = 0}  洞的半径
- @param drillDistance ?: {x:number = 0,y:number = 0}  开始钻孔的距离
- @param effectRange ?: {x:number = Number.POSITIVE_INFINITY,y:number = Number.POSITIVE_INFINITY}  有效范围，影响范围
- 说明：钻洞转换器生成器



## rightAngleEdgeCreater
`rightAngleEdgeCreater(hypotenuse,overflowValue)`
- @param hypotenuse ?: {x:number = 0 ,y:number = 0}  直角三角形斜边的长
- @param overflowValue ?: number  当坐标距离的绝对值大于 hypotenuse 时的转换结果值；默认为原始坐标距离；
- 说明：直角邻边转换器生成器



## hypotenuse
- 说明：三角形斜边斜边转换器，计算以distance的x和y为直角边的三角形的斜边的长；


## isCrossOrContainItemRectCreater
`isCrossOrContainItemRectCreater(compriseReact)`
- @param compriseReact :{x:number,y:number,width:number,height:number}  包含者的矩形
- 说明：判断是否包含项目矩形的转换器生成器
**注意：**  
判断结果保存在 distanceArr.publicData.isCrossOrContainItemRect


## affecterIsCrossOrContainItemRect
- 说明：判断Affecter的视口是否包含项目矩形的转换器

**注意:**  
判断结果保存在 distanceArr.publicData..affecterIsCrossOrContainItemRect



## maxXYIndex
- 说明：寻找距离坐标中最大的 x 和 y
**注意:**  
结果保存在 publicData.maxX 和 publicData.maxY 中；



## minXYIndex
- 说明：寻找距离坐标中最小的 x 和 y
**注意:**  
结果保存在 publicData.minX 和 publicData.minY 中