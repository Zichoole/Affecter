
[本项目的Git仓库]: https://gitee.com/guobinyong/Affecter.git
[Affecter的教程]: https://gitee.com/guobinyong/Affecter
[内置Transforms的接口文档]: https://gitee.com/guobinyong/Affecter/blob/master/doc/内置Transforms的接口文档.md


# 一、简介
affecter-transforms 是一个关于 affecter 的转换器库，里面有常用的affecter转换器！

详情请参考《[Affecter的教程][]》 和 《[内置Transforms的接口文档][]》

**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe





# 二、安装方式
目前，安装方式有以下作几种：


## 方式1：通过 npm 安装
```
npm install --save affecter-transforms
```


## 方式2：直接下载原代码
您可直接从 [本项目的Git仓库][] 下载，然后直接把 [本项目的Git仓库][] 下的 transforms-pack/Transforms.js 文件拷贝到您的项目中去，并通过如下代码在您的项目中引入您需要使用的转换器：
```
import { 您需要的转换器,您需要的转换器 } from "path/to/Transforms.js";
```